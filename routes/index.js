var express = require('express');
var router = express.Router();
var mongoose  = require('mongoose');
var User      = mongoose.model('Users');
var bcrypt = require('bcryptjs');
const { check, validationResult } = require('express-validator/check');
const { matchedData, sanitize }   = require('express-validator/filter');
var request = require('request');
var csrf = require('csurf');
var bodyParser = require('body-parser');
// setup route middlewares
var csrfProtection = csrf({ cookie: true })
var parseForm = bodyParser.urlencoded({ extended: false });

var country_latlong = require('../countrycode-latlong.min.json');
var country_names = require('../countrycode-names.json');
var generator = require('generate-password');
var nodemailer = require("nodemailer");
//let aws = require('aws-sdk');
//aws.config.loadFromPath('./aws-config.json');
var from_address = 'wcst.co@gmail.com';
//var from_address = 'info@dsfcount.pw';
var transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: 'wcst.co@gmail.com',
    pass: '{%|4s_,2T[{~&.w'
  }
});

// let transporter = nodemailer.createTransport({
//   SES: new aws.SES({
//       apiVersion: '2010-12-01'
//   })
// });

function countUsers(req, res, next) {
  User.countDocuments({ email: { $ne: 'deleted' } }, function( err, count){
    if (err) throw err;
    req.countusers = count;
    return next();
  })
}

function findWorldCountry(req, res, next) {
  User.aggregate([
    { $match: { email: { $ne: 'deleted' } } },
    { $group: { _id: '$country', count: {$sum: 1} } },
  ], function (err, results) {
    if (err) throw err;
    req.worldcountry = results;
    return next();
  });
}

function findTopTen(req, res, next) {
  User.aggregate([
    { $match: { email: { $ne: 'deleted' } } },
    { $group: { _id: '$country', count: {$sum: 1} } },
    { $sort:{"count":-1} },
    { $limit: 10 },
  ], function (err, results) {
    if (err) throw err;
    req.toptens = results;
    return next();
  });
}

function countGenderGroups(req, res, next) {
  User.aggregate([
    { $match: { email: { $ne: 'deleted' } } },
    { $project: {
        male: {$cond: [{$eq: ["$gender", "m"]}, 1, 0]},
        female: {$cond: [{$eq: ["$gender", "f"]}, 1, 0]},
        transmale: {$cond: [{$eq: ["$gender", "tm"]}, 1, 0]},
        transfemale: {$cond: [{$eq: ["$gender", "tf"]}, 1, 0]},
        other: {$cond: [{$eq: ["$gender", "ot"]}, 1, 0]},
        none: {$cond: [{$eq: ["$gender", "na"]}, 1, 0]},
      }
    },
    { $group: { 
        _id: null,
        male: {$sum: "$male"},
        female: {$sum: "$female"},
        transmale: {$sum: "$transmale"},
        transfemale: {$sum: "$transfemale"},
        other: {$sum: "$other"},
        none: {$sum: "$none"},
      } 
    }
  ], function (err, results) {
    if (err) throw err;
    req.gender_groups = results;
    return next();
  });
}

function countAgeGroups(req, res, next) {
  User.aggregate([
    { $match: { email: { $ne: 'deleted' } } },
    { $project: {
        "_id":0, 
        "age": { $divide: [ { $subtract: [ new Date(), { $ifNull: ["$dob", new Date() ]} ] }, 1000 * 86400 * 365 ] }
      }
    },
    { $project: {    
        "range": {
           $concat: [
              { $cond: [{$lte: ["$age",0]}, "unknown", ""]}, 
              { $cond: [{$and:[ {$gt:["$age", 0 ]}, {$lt: ["$age", 30]}]}, "under_30", ""] },
              { $cond: [{$and:[ {$gte:["$age", 31]}, {$lt:["$age", 46]}]}, "age_31_45", ""]},
              { $cond: [{$and:[ {$gte:["$age", 46]}, {$lt:["$age", 61]}]}, "age_46_60", ""]},
              { $cond: [{$gte:["$age", 61]}, "over_61", ""]}
           ]
        }  
      }    
    },
    { $group: { "_id" : "$range", count: { $sum: 1 } } }
  ], function (err, results) {
    if (err) throw err;
    req.age_groups = results;
    return next();
  });
}

function countReason(req, res, next) {
  User.countDocuments({ reason: { $eq: 'reason2' } }, function( err, count){
    if (err) throw err;
    req.countreason = count;
    return next();
  })
}

function renderIndexPage(req, res) {
  res.render('index', {
    page: 'Home',
    countusers: req.countusers,
    worldcountry: req.worldcountry,
    toptens: req.toptens,
    gender_groups: req.gender_groups,
    age_groups: req.age_groups,
    country_latlong: country_latlong,
    country_names: country_names,
    countreason: req.countreason,
    csrfToken: req.csrfToken()
  });
}

router.get('/', csrfProtection, countUsers, findWorldCountry, findTopTen, countGenderGroups, countAgeGroups, countReason, renderIndexPage);

router.get('/privacy', csrfProtection, function(req, res, next) {
  res.render('privacy', {page:'Privacy Policy', csrfToken: req.csrfToken()});
});

router.get('/terms', csrfProtection, function(req, res, next) {
  res.render('terms', {page:'Terms & Conditions', csrfToken: req.csrfToken()});
});

router.get('/cookies', csrfProtection, function(req, res, next) {
  res.render('cookies', {page:'Cookies Policy', csrfToken: req.csrfToken()});
});

router.get('/success', csrfProtection, function(req, res, next) {
  res.render('success', {page:'Success', csrfToken: req.csrfToken()});
});

router.get('/signup', csrfProtection, function(req, res, next) {
  if(req.session.user) res.redirect('/account');
  res.render('signup', {page:'Sign Up', csrfToken: req.csrfToken()});
});

router.post('/signup', parseForm, csrfProtection, [ 
  check('name', 'Name cannot be left blank').trim().isLength({ min: 1 }).isLength({ max: 50 }).withMessage('Name is less than 50 characters long.'),
  check('email').isEmail().withMessage('Please enter a valid email address').trim().isLength({ max: 255 }).withMessage('Email is less than 255 characters long.')
  .custom(value => {
    return new Promise((resolve, reject) => {
      User.findOne({ email: value })
        .exec((err, doc) => {
          if (err) return reject(err)
          if (doc) return reject(new Error('This email already exists. Please enter another email.'))
          else return resolve(value)
        })
    })
  }),
  check('gender','Please select gender').isLength({ min: 1 }),
  check('dob_days', 'Please select day').isLength({ min: 1 }),
  check('dob_months', 'Please select month').isLength({ min: 1 }),
  check('dob_years', 'Please select year').isLength({ min: 1 }),
  check('country','Country cannot be left blank').isLength({ min: 1 }),
  check('state')
  .custom((value, {req}) => {
    if(req.body['country'] === 'US' && value === '') throw new Error('Please select state!');
    else return true;
  }),
  check('city').isLength({ max: 30 }).withMessage('City is less than 30 characters long.'),
  check('start_year','Year required').isLength({ min: 1 }),
  check('reason','Please select reason').isLength({ min: 1 }),
  check('counted_bcz')
  .custom((value, {req}) => {
    if(req.body['country'] === 'US' && value === '') throw new Error('Please select atleast one option!');
    else return true;
  }),
  check('o_counted_bcz')
  .custom((value, {req}) => {
    if(req.body['counted_bcz'].split(',').includes("o") && value === ''){
      throw new Error('Reason cannot be left blank');
    } else {
      return true;
    }
  }),
  check('hear_about_us')
  .custom((value, {req}) => {
    if(req.body['country'] === 'US' && value === '') throw new Error('Please select atleast one option!');
    else return true;
  }),
  check('g-recaptcha')
  .custom((value, {req}) => {
    if(req.session['recaptchaSuccess'] == 1){
      return true;
    }
    if(req.body['g-recaptcha'] === undefined || req.body['g-recaptcha'] === '' || req.body['g-recaptcha'] === null){
      throw new Error('Please select captcha');
    } else {
      return true;
    }
    return new Promise((resolve, reject) => {
      var secretKey = "6LejCIsUAAAAAC46h8uxbX5LEYKw9UgKtaGMcmJy";
      var verificationUrl = "https://www.google.com/recaptcha/api/siteverify?secret=" + secretKey + "&response=" + value + "&remoteip=" + req.connection.remoteAddress;
      request(verificationUrl, function(error, response, body) {
        body = JSON.parse(body);
        if(body.success !== undefined && !body.success) {
          return reject(new Error('Captcha validation failed'))
        } else {
          if (!req.session['recaptchaSuccess']) {
            req.session['recaptchaSuccess'] = 1;
          }
          return resolve(value);
        }
      });
    })
  }),
 ], function(req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {    
    res.json({status : "error", message : errors.array()});
  } else {
    var password = generator.generate({
      length: 10,
      numbers: true,
      symbols: true
    });
    var encpassword = bcrypt.hashSync(password, 10);

    User.findOne().sort({'_id': -1}).exec(function(err, doc) {
      if (err) throw err;
      state = 'nostate';
      if (req.body.country == 'US') state = req.body.state;
      var document = {
        name:       req.body.name, 
        email:      req.body.email,
        password:   encpassword,
        gender:     req.body.gender,
        dob:        new Date(req.body.dob_years, req.body.dob_months - 1, req.body.dob_days),
        city:       req.body.city,
        country:    req.body.country,
        state:      state,
        start_year: req.body.start_year,
        start_month:req.body.start_month,
        start_day:  req.body.start_day,
        reason:     req.body.reason,
        counted_bcz: req.body.counted_bcz.split(','),
        o_counted_bcz: req.body.o_counted_bcz,
        subscribed: ["st", "nw", "bl", "pm", "pc", "ac"],
        hear_about_us: req.body.hear_about_us.split(','),
        created_at: new Date(),
      };

      if (!doc) {
        document.user_number = 1;
      } else {
        document.user_number = doc.user_number + 1;
      }

      var user = new User(document); 
      user.save(function(error){
        if(error){ 
          throw error;
        }
        res.render('registerEmail', {base_url: req.protocol + '://' + req.get('host') + req.baseUrl, name: req.body.name, email: req.body.email, password: password, user_number: document.user_number}, function(err, html){
          if(err) throw err;
          const mailOptions = {
            from: from_address,
            to: req.body.email,
            subject: 'New account is created of dsfcount',
            html: html
          };
          transporter.sendMail(mailOptions, function (err, info) {
            if(err) throw err;
            req.session['recaptchaSuccess'] = 0;
            res.json({status : "success"});
          });
        });
      }); 
    });   
  }
});

router.post('/login', parseForm, csrfProtection, [ 
  check('login_email').isEmail().withMessage('Please enter a valid email address').trim(),
  check('login_password','Password cannot be left blank').isLength({ min: 1 }),
 ], function(req, res, next) {
  if (!req.session['mailAttempts']) {
    req.session['mailAttempts'] = 1;
  } else if(req.session['mailAttempts'] > 2){
    return res.json({status : "error", message : [{"param": "login_email", "msg": "3 Attempts finished. You have to reset your password!"}], reset_email: true});
  }
  const errors = validationResult(req);
  if (!errors.isEmpty()) {    
    res.json({status : "error", message : errors.array()});
  } else {
    User.findOne({email: req.body.login_email})
      .exec((err, doc) => {
        if (err) throw err;
        if (!doc) {
          req.session['mailAttempts'] = req.session['mailAttempts'] + 1;
          res.json({status : "error", message : [{"param": "login_email", "msg": "Wrong username or password!"}], reset_email: false});
        } else {
          if(bcrypt.compareSync(req.body.login_password, doc.password)) {
            req.session.user = doc;
            req.session['mailAttempts'] = 1;
            res.json({status : "success"});
          } else {
            req.session['mailAttempts'] = req.session['mailAttempts'] + 1;
            res.json({status : "error",message : [{"param": "login_email", "msg": "Wrong username or password!"}], reset_email: false});
          }
        }
    })
  }
});

router.get('/account', csrfProtection, function(req, res, next) {
  if(req.session.user != undefined) {
    User.findById(req.session.user._id)
      .exec(function (error, user) {
        if (error) {
          return next(error);
        } else {
          if (user === null) {
            var err = new Error('Not authorized! Go back!');
            err.status = 400;
            return next(err);
          } else {
            res.render('account', {page: 'Account', csrfToken: req.csrfToken(), user_data: user, messages: req.flash('success')});
          }
        }
      });
  } else {
    res.render('account', {page: 'Account', csrfToken: req.csrfToken()});
  }
});

router.get('/logout', function (req, res, next) {
  if (req.session) {
    req.session.destroy(function (err) {
      if (err) {
        return next(err);
      } else {
        return res.redirect('/');
      }
    });
  }
});

function checkSignIn(req, res, next){
  if(req.session.user){
     next();
  } else {
    res.redirect('/');
  }
}

router.post('/account', parseForm, csrfProtection, [ 
  check('name', 'Name cannot be left blank').trim().isLength({ min: 1 }).isLength({ max: 50 }).withMessage('Name is less than 50 characters long.'),
  check('gender','Please select gender').isLength({ min: 1 }),
  check('dob_days', 'Please select day').isLength({ min: 1 }),
  check('dob_months', 'Please select month').isLength({ min: 1 }),
  check('dob_years', 'Please select year').isLength({ min: 1 }),
  check('country','Country cannot be left blank').isLength({ min: 1 }),
  check('state')
  .custom((value, {req}) => {
    if(req.body['country'] === 'US' && value === '') throw new Error('Please select state!');
    else return true;
  }),
  check('start_year','Year required').isLength({ min: 1 }),
  check('reason','Please select reason').isLength({ min: 1 }),
  check('counted_bcz')
  .custom((value, {req}) => {
    if(req.body['country'] === 'US' && value === '') throw new Error('Please select atleast one option!');
    else return true;
  }),
  check('o_counted_bcz')
  .custom((value, {req}) => {
    if(req.body['counted_bcz'].split(',').includes("o") && value === ''){
      throw new Error('Reason cannot be left blank');
    } else {
      return true;
    }
  }),
  check('current_password').trim()
  .custom((value, {req}) => {
    if(value != '') {
      return new Promise((resolve, reject) => {
        User.findById(req.session.user._id)
         .exec((err, doc) => {
           if (err) return reject(err)
           if (doc) {
            if(bcrypt.compareSync(value, doc.password)) {
              return resolve(value);
            } else {
              return reject(new Error('Wrong password'));
            }
           } else {
            return reject(new Error('Error.'))
           }
        })
      })
    } else {
      return true;
    }
  }),
  check('new_password').trim()
  .custom((value, {req}) => {
    if(req.body.current_password != '') {
      if(value.length < 6){
        throw new Error('Atleast 6 character!');
      } else return true;
    } else {
      return true;
    }
  }),
  check('c_new_password').trim()
  .custom((value, {req}) => {
    if(req.body.new_password != '' && value != req.body.new_password) {
      throw new Error('New password cannot match!');
    } else {
      return true;
    }
  }),
  check('hear_about_us')
  .custom((value, {req}) => {
    if(req.body['country'] === 'US' && value === '') throw new Error('Please select atleast one option!');
    else return true;
  }),
 ], function(req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {    
    res.json({status : "error", message : errors.array()});
  } else {
    state = 'nostate';
    if (req.body.country == 'US') state = req.body.state;
    var document = {
      name:       req.body.name,
      gender:      req.body.gender,
      dob:      new Date(req.body.dob_years, req.body.dob_months - 1, req.body.dob_days),
      city:      req.body.city,
      country:      req.body.country,
      state:      state,
      start_year:      req.body.start_year,
      start_month:      req.body.start_month,
      start_day:      req.body.start_day,
      reason:      req.body.reason,
      counted_bcz: req.body.counted_bcz.split(','),
      o_counted_bcz: req.body.o_counted_bcz,
      subscribed: req.body.subscribed.split(','),
      hear_about_us: req.body.hear_about_us.split(','),
    };

    if(req.body.new_password.trim() != '') {
      document.password = bcrypt.hashSync(req.body.new_password, 10);
    }

    User.findOneAndUpdate({ '_id': req.session.user._id }, document, function(err, result){
      if(err) throw error;
      req.flash('success', 'Information is updated!');
      if(req.body.new_password.trim() != '') {
        res.render('changePasswordEmail', {name: req.body.name}, function(err, html){
          if(err) throw err;
          const mailOptions = {
            from: from_address,
            to: req.session.user.email,
            subject: 'Your password is changed for dsfcount',
            html: html
          };
          transporter.sendMail(mailOptions, function (err, info) {
            if(err) throw err;
            res.json({status : "success"});
          });
        });
      } else {
        res.render('changeAccountEmail', {name: req.body.name}, function(err, html){
          if(err) throw err;
          const mailOptions = {
            from: from_address,
            to: req.session.user.email,
            subject: 'Your account detsil is changed for dsfcount',
            html: html
          };
          transporter.sendMail(mailOptions, function (err, info) {
            if(err) throw err;
            res.json({status : "success"});
          });
        });
      }
    });
  }
});

router.post('/delete-account', parseForm, csrfProtection, function (req, res, next) {
  if (req.session) {
    User.findById(req.session.user._id)
      .exec(function (error, user) {
        if (error) throw error;
        let email = user.email;
        let name = user.name;
        let document = {
          name: 'deleted',
          email: 'deleted',
        };
        if(req.body.delete_reason == 'not_staying') document.reason = 'none';
        User.findByIdAndUpdate(req.session.user._id, document, function(err, result){
          if(err) throw error;
          res.render('deleteAccountEmail', {name: name}, function(err, html){
            if(err) throw err;
            const mailOptions = {
              from: from_address,
              to: email,
              subject: 'Your account is deleted for dsfcount',
              html: html
            };
            transporter.sendMail(mailOptions, function (err, info) {
              if(err) throw err;
              res.json({status : "success"});
            });
          });
        });
      });
  }
});

router.post('/reset-password', parseForm, csrfProtection, [
  check('reset_email').isEmail().withMessage('Please enter a valid email address').trim()
  .custom(value => {
    return new Promise((resolve, reject) => {
      User.findOne({ email: value })
        .exec((err, doc) => {
          if (err) return reject(err)
          if (!doc) return reject(new Error('This email is not exists.'))
          else return resolve(value)
        })
    })
  }),
 ], function(req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {    
    res.json({status : "error", message : errors.array()});
  } else {
    var password = generator.generate({
      length: 10,
      numbers: true,
      symbols: true
    });
    var bcrypt_password = bcrypt.hashSync(password, 10);
    var query = { 'email': req.body.reset_email };
    User.findOneAndUpdate(query, {password: bcrypt_password}, function(err, doc){
      if(err) throw err;
      res.render('resetPasswordEmail', {password: password}, function(err, html){
        if(err) throw err;
        const mailOptions = {
          from: from_address,
          to: req.body.reset_email,
          subject: 'New password of dsfcount',
          html: html
        };
        transporter.sendMail(mailOptions, function (err, info) {
          if(err) throw err;
          req.session['mailAttempts'] = 1;
          res.json({status : "success"});
        });
      });
    });
  }
});

router.get('/user-count', function (req, res, next) {
  if (req.session) {
    User.countDocuments({ email: { $ne: 'deleted' } }, function( err, count){
      res.json({status : "success", users: count});
    })
  }
});

router.get('/unsubscribe', csrfProtection, function(req, res, next) {
  if(req.session.user) res.redirect('/account');
  res.render('unsubscribe', {page:'Unsubscribe', csrfToken: req.csrfToken(), messages: req.flash('success')});
});

router.post('/unsubscribe', parseForm, csrfProtection, [ 
  check('email').isEmail().withMessage('Please enter a valid email address').trim()
  .custom(value => {
    return new Promise((resolve, reject) => {
      User.findOne({ email: value })
        .exec((err, doc) => {
          if (err) return reject(err)
          if (!doc) return reject(new Error('This email is not exists.'))
          else return resolve(value)
        })
    })
  }),
 ], function(req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {    
    res.json({status : "error", message : errors.array()});
  } else {
    if(req.body.subscribed) subscribed = req.body.subscribed.split(',');
    else subscribed = [];

    var document = {
      subscribed: subscribed,
    };
    User.findOneAndUpdate({ email: req.body.email }, document, function(err, result){
      if(err) throw error;
      req.flash('success', 'Subscription is updated!');
      res.json({status : "success"});
    });
  }
});

module.exports = router;