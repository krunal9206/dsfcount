$(function() {
    $("#account-form").submit(function(e) {
        e.preventDefault();
        var subscribed = [];
        $('.subscribed:checked').each(function() {
            subscribed.push($(this).val());
        });
        subscribed.push("ac");
        var counted_bcz = [];
        $('.counted_bcz:checked').each(function() {
            counted_bcz.push($(this).val());
        });
        var o_counted_bcz = '';
        if(counted_bcz.includes("o")) {
            o_counted_bcz = $('#o_counted_bcz').val();
        }
        var hear_about_us = [];
        $('.hear_about_us:checked').each(function() {
            hear_about_us.push($(this).val());
        });
        $.ajax({
			url: "/account",
			method: "POST",
			data: {
                name: $('#name').val(),
                email: $('#email').val(),
                gender: $('#gender').val(),
                dob_days: $('#dob_days').val(),
                dob_months: $('#dob_months').val(),
                dob_years: $('#dob_years').val(),
                city: $('#city').val(),
                country: $('#country').val(),
                state: $('#state').val(),
                start_year: $('#start_year').val(),
                start_month: $('#start_month').val(),
                start_day: $('#start_day').val(),
                reason: $('input[name="reason"]:checked').val(),
                counted_bcz: counted_bcz.join(','),
                o_counted_bcz: o_counted_bcz,
                current_password: $('#current_password').val(),
                new_password: $('#new_password').val(),
                c_new_password: $('#c_new_password').val(),
                subscribed: subscribed.join(','),
                hear_about_us: hear_about_us.join(','),
                _csrf : $('meta[name="csrf-token"]').attr('content')
            },
            beforeSend: function() {
                $('#submit-btn').prop('disabled', true);
            },
			dataType: "json",
		}).done(function( data ) {
            $('#submit-btn').prop('disabled', false);
            $('.invalid-feedback').remove();
            if ( data ) {
                if(data.status == 'error') {
                    var i = 0;
                    var first_key = '';
                    $.each( data.message, function( key, value ) {
                        if(i == 0) first_key = value.param;
                        $('#' + value.param).parent('div').append('<div class="invalid-feedback">' + value.msg + '</div>');
                        i++;
                    });
                    $('#' + first_key).focus();
                } else {
                    location = '/account';
                }
            }
        });
    });

    var socket = io();

    $("#delete-btn").click(function() {
        $('#deleteModal').modal('show');
    });

    $("#delete-account-form").submit(function(e) {
        e.preventDefault();
        $.ajax({
            url: "/delete-account",
            method: "POST",
            data: {
                delete_reason: $('input[name="delete_reason"]:checked').val(),
                _csrf : $('meta[name="csrf-token"]').attr('content')
            },
            beforeSend: function() {
                $('#delete-account-form button').prop('disabled', true);
            },
            dataType: "json",
        }).done(function( data ) {
            $('#delete-account-form button').prop('disabled', false);
            if ( data ) {
                if(data.status == 'success') {
                    socket.emit('krunal_dsfcount', 'delete');
                    location = '/logout';
                }
            }
        });
    });

    $("#cancel-btn").click(function() {
        $("#account-form input, #account-form select, #account-form button, #account-form checkbox").prop('disabled', true);

        $("#delete-btn").prop('disabled', false);
    });

    $("#edit-btn").click(function() {
        $("#account-form input, #account-form select, #account-form button, #account-form checkbox").prop('disabled', false);

        $('#account-form .selectpicker').selectpicker('refresh');
    });
});