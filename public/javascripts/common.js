$(function() {
    window.addEventListener("load", function(){
        window.cookieconsent.initialise({
            content: {
                header: 'Cookies used on the website!',
                message: 'This website uses cookies to ensure you get the best experience on our website.',
                dismiss: 'Got it!',
                allow: 'Allow cookies',
                deny: 'Decline',
                link: 'Learn more',
                href: '/cookies',
                close: '&#x274c;',
                policy: 'Cookie Policy',
                target: '_self',
            },
            "palette": {
                "popup": {
                    "background": "#000"
                },
                "button": {
                    "background": "#f1d600"
                }
            }
        })
    });
    $("#login-form").submit(function(e) {
        e.preventDefault();
        $.ajax({
			url: "/login",
			method: "POST",
			data: {
                login_email: $('#login_email').val(),
                login_password: $('#login_password').val(),
                _csrf : $('meta[name="csrf-token"]').attr('content')
            },
            beforeSend: function() {
                $('#login-form button').prop('disabled', true);
            },
			dataType: "json",
		}).done(function( data ) {
            $('#login-form button').prop('disabled', false);
            $('#login-form .invalid-feedback').remove();
            if ( data ) {
                if(data.status == 'error') {
                    $.each( data.message, function( key, value ) {
                        $('#' + value.param).after('<div class="invalid-feedback">' + value.msg + '</div>');
                    });
                    $('#login_password').val('');
                    if(data.reset_email) { 
                        $('#login-form').hide(); 
                        $('#reset-password-form').show();
                        $('#reset_email').val($('#login_email').val());
                    }
                } else {
                    location = '/account';
                }
            }
        });
    });

    $("#reset-password-form").submit(function(e) {
        e.preventDefault();
        $.ajax({
			url: "/reset-password",
			method: "POST",
			data: {
                reset_email: $('#reset_email').val(),
                _csrf : $('meta[name="csrf-token"]').attr('content')
            },
            beforeSend: function() {
                $('#reset-password-form button').prop('disabled', true);
            },
			dataType: "json",
		}).done(function( data ) {
            $('#reset-password-form button').prop('disabled', false);
            $('#reset-password-form .invalid-feedback').remove();
            if ( data ) {
                if(data.status == 'error') {
                    $.each( data.message, function( key, value ) {
                        $('#' + value.param).after('<div class="invalid-feedback">' + value.msg + '</div>');
                    });
                } else {
                    location = '/success';
                }
            }
        });
    });
});