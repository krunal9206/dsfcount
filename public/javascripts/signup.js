var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September','October', 'November', 'December'];

function updateNumberOfDays(){
    dob_days = $('#dob_days').val();
    $('#dob_days').html('');
    month = $('#dob_months').val();
    year = $('#dob_years').val();
    days = daysInMonth(month, year);
    $('#dob_days').append($('<option />').val('').html('Select Day'));
    for(i=1; i < days+1 ; i++){
        $('#dob_days').append($('<option />').val(i).html(i));
    }
    $('#dob_days').val(dob_days);
}

function updateNumberOfDays1(){
    start_day = $('#start_day').val();
    $('#start_day').html('');
    month = $('#start_month').val();
    year = $('#start_year').val();
    days = daysInMonth(month, year);
    if(year == new Date().getFullYear() && month == new Date().getMonth() + 1) days = new Date().getDate();
    $('#start_day').append($('<option />').val('').html('Select Day'));
    for(i=1; i < days+1 ; i++){
        $('#start_day').append($('<option />').val(i).html(i));
    }
    $('#start_day').val(start_day);
}

function updateNumberOfMonth1(){
    start_month = $('#start_month').val();
    $('#start_month').html('');
    year = $('#start_year').val();
    $('#start_month').append($('<option />').val('').html('Select Month'));

    from_month = 13;
    if(year == new Date().getFullYear()) from_month = new Date().getMonth() + 2;
    for (i = 1; i < from_month; i++){
        $('#start_month').append($('<option />').val(i).html(months[i - 1]));
    }
    $('#start_month').val(start_month);
}

function daysInMonth(month, year) {
    return new Date(year, month, 0).getDate();
}
$(function() {
    $('#dob_years, #start_year').append($('<option />').val('').html('Select Year'));
    for (i = new Date().getFullYear() - 13; i > new Date().getFullYear() - 106; i--){
        $('#dob_years').append($('<option />').val(i).html(i));
    }

    for (i = new Date().getFullYear(); i > new Date().getFullYear() - 106; i--){
        $('#start_year').append($('<option />').val(i).html(i));
    }
    
    $('#dob_months, #start_month').append($('<option />').val('').html('Select Month'));
    for (i = 1; i < 13; i++){
        $('#dob_months, #start_month').append($('<option />').val(i).html(months[i - 1]));
    }

    updateNumberOfDays(); updateNumberOfDays1(); 

    $('#dob_years, #dob_months').change(function(){
        updateNumberOfDays(); 
    });

    $('#start_year, #start_month').change(function(){
        updateNumberOfDays1(); 
    });

    $('#start_year').change(function(){
        updateNumberOfMonth1(); 
    });

    $('#country').on('change', function() {
        $('#state-area').hide();
        if($(this).val() == 'US') $('#state-area').show();
    });

    var socket = io();

    $("#signup-form").submit(function(e) {
        e.preventDefault();
        var counted_bcz = [];
        $('.counted_bcz:checked').each(function() {
            counted_bcz.push($(this).val());
        });

        var o_counted_bcz = '';
        if(counted_bcz.includes("o")) {
            o_counted_bcz = $('#o_counted_bcz').val();
        }

        var hear_about_us = [];
        $('.hear_about_us:checked').each(function() {
            hear_about_us.push($(this).val());
        });
        
        $.ajax({
            url: "/signup",
            method: "POST",
            data: {
                name: $('#name').val(),
                email: $('#email').val(),
                gender: $('#gender').val(),
                dob_days: $('#dob_days').val(),
                dob_months: $('#dob_months').val(),
                dob_years: $('#dob_years').val(),
                city: $('#city').val(),
                country: $('#country').val(),
                state: $('#state').val(),
                start_year: $('#start_year').val(),
                start_month: $('#start_month').val(),
                start_day: $('#start_day').val(),
                reason: $('input[name="reason"]:checked').val(),
                counted_bcz: counted_bcz.join(','),
                o_counted_bcz: o_counted_bcz,
                hear_about_us: hear_about_us.join(','),
                'g-recaptcha': grecaptcha.getResponse(),
                _csrf : $('meta[name="csrf-token"]').attr('content')
            },
            beforeSend: function() {
                $('#submit-btn').prop('disabled', true);
            },
            dataType: "json",
        }).done(function( data ) {
            $('#submit-btn').prop('disabled', false);
            $('.invalid-feedback').remove();
            if ( data ) {
                if(data.status == 'error') {
                    var i = 0;
                    var first_key = '';
                    $.each( data.message, function( key, value ) {
                        if(i == 0) first_key = value.param;
                        $('#' + value.param).parent('div').append('<div class="invalid-feedback">' + value.msg + '</div>');
                        i++;
                    });
                    $('#' + first_key).focus();
                } else {
                    socket.emit('krunal_dsfcount', 'create');
                    location = '/success';
                }
            }
        });
    });

    $('.selectpicker').selectpicker();
});