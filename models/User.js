var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var userSchema = new Schema({
  name: String,
  email: String,
  password: String,
  gender: String,
  dob: Date,
  city: String,
  country: String,
  state: String,
  start_year: Number,
  start_month: Number,
  start_day: Number,
  reason: String,
  counted_bcz: [String],
  o_counted_bcz: String,
  subscribed: [String],
  hear_about_us: [String],
  user_number: Number,
  created_at: Date,
});

module.exports = mongoose.model('Users', userSchema);